#!/usr/bin/python
# -*- coding: utf-8 -*-

import testutils

def main():
    testutils.run_server(port = 9999)

if __name__ == "__main__":
    main();
