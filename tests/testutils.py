#!/usr/bin/python
# -*- coding: utf-8 -*-

import random
import socket
import struct
import threading
import time

# Message #1 (fixed size)
# [uint8]  : packet start = 0x42 'B'
# [uint8]  : packet type  = 0x31 '1'
# [uint32] : clientID (network byte order)

# Message #2
# [uint8]  : packet start = 0x42 'B'
# [uint8]  : packet type  = 0x32 '2'
# [uint32] : data length (network byte order)
# [bytes]  : data

def run_client(clientId):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(("example.com", 8888))

    # Message #1
    msg1 = struct.pack("!BBI", 
                       ord('B'), # packet start
                       ord('1'), # packet type
                       clientId) # cleint ID
    s.send(msg1)
    print("[client #{}] msg#1: clientID={}".format(clientId, clientId))

    for i in range(1000): 
        data_len = random.randint(15, 32)
        data = "{:0>{width}}".format(i, width=data_len)

        msg2 = struct.pack("!BBI", 
                           ord('B'), # packet start
                           ord('2'), # packet type
                           data_len)
        msg2 += data
        s.send(msg2)
        print("[client #{}] msg#2: data_length={:0>2} data='{}'".format(clientId, data_len, data))

       	time.sleep(1)

    s.close()

def handle_incoming_connection(serverId, socket):
    parser = Parser(serverId)
    while True:
        d = socket.recv(1024)
        if not d:
            break;
        
        if not parser.parse_data(d):
            break;

    socket.close()

def run_server(port):
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind(("0.0.0.0", port))

    while True:
        server_socket.listen(5)
        client_socket, client_address = server_socket.accept()

	# without comma you'd get a... TypeError: handle_client_connection() argument after * must be a sequence, not _socketobject
        client_handler = threading.Thread(target=handle_incoming_connection, args=(port, client_socket,))  
        client_handler.start()

    server_socket.close()

class Parser:
    def __init__(self, serverId):
	self.serverId = serverId
        self.data = ""
        self.data_to_skip = 0
        
    def parse_data(self, data):
        self.data += data

        while True:
            
            if self.data_to_skip == 0:
                if len(self.data) < 6:
                    return True
                
                packet_start = 0
                packet_type = 0
                int_value = 0
                packet_start, packet_type, int_value = struct.unpack("!BBI", self.data[:6])
        
                if packet_start != ord('B'):
                    print("[server #{}] Invalid packet start symbol".format(self.serverId))
                    return False
        
                if packet_type != ord('1') and packet_type != ord('2'):
                    print("[server #{}] Invalid packet start symbol".format(self.serverId))
                    return False
            
                self.data = self.data[6:]
                
                if packet_type == ord('1'):
                    print("[server #{}] msg#1: clientID={}".format(self.serverId, int_value))
                else: # if packet_type != org('1'):
                    self.data_to_skip = int_value

            if self.data_to_skip <= len(self.data):
                print("[server #{}] msg#2: data_length={:0>2} data='{}'".format(self.serverId, self.data_to_skip, self.data[: self.data_to_skip]))
                self.data = data = self.data[self.data_to_skip:]
                self.data_to_skip = 0
            else:
                return True
