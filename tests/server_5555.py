#!/usr/bin/python
# -*- coding: utf-8 -*-

import testutils

def main():
    testutils.run_server(port = 5555)

if __name__ == "__main__":
    main();
