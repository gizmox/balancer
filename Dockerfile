FROM ubuntu:18.04

RUN apt-get -y update
RUN apt-get -y install python libboost-all-dev liblog4cplus-dev

ADD ./build/balancer /app/balancer
ADD ./log4cplus.ini  /app/log4cplus.ini
ADD ./routetable.txt /app/routetable.txt
ADD ./tests/*.py     /app/
ADD ./docker/*       /app/

WORKDIR /app

CMD "/app/run.sh"