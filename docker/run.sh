#!/bin/sh

echo "127.0.0.1 example.com" >> /etc/hosts
echo "127.0.0.1 n1.example.com" >> /etc/hosts
echo "127.0.0.1 n2.example.com" >> /etc/hosts
echo "127.0.0.1 n3.example.com" >> /etc/hosts

cd /app

./balancer log4cplus.ini routetable.txt &
./server_5555.py &
./server_9999.py &
./client_1.py &
./client_2.py &
./client_3.py &
sleep 1d
