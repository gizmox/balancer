# Dependencies #

Following packages are to be installed in order to build and run the project in docker:

* cmake:         `$ sudo apt -y install cmake`
* docker:
    refer to [Get Docker CE for Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/) and [Post-installation steps for Linux](https://docs.docker.com/install/linux/linux-postinstall/)
* boost library: `$ sudo apt install libboost-all-dev`
    
The rest of dependencies are required to build and run project locally:


* python 2.7:    `$ sudo apt install python`
* following Python modiles are required:

```
        random
        socket
        struct
        threading
        time
```

# Building the project #

Being in `balancer` repository root directory do the following:

* create `build` directory:

```
        <repo-dir> $ mkdir build
        <repo-dir> $ cd build
```

* build the project:

```
        <repo-dir>/build $ cmake ..
        <repo-dir>/build $ make
```

# Runing `balancer` #

* run `balancer`:

```
        <repo-dir>/build $./balancer ../log4cplus.ini ../routetable.txt
```    

# Running functional tests #

Append your `/etc/hosts` with following lines in order to let host names for tests to be resoved to 127.0.0.1:

```
    127.0.0.1 exaple.com
    127.0.0.1 n1.exaple.com
    127.0.0.1 n2.exaple.com
    127.0.0.1 n3.exaple.com
```

Run following executables in separate terminals:

(current directory is the repository root directory)

* run `balancer` (in separate terminal window):

```
        <repo-dir> $ ./build/balancer log4cplus.ini routetable.txt
```    

* run test servers (in separate terminal windows):

```
        <repo-dir> $ ./tests/server_5555.py
        <repo-dir> $ ./tests/server_9999.py
```

* run test clients (in separate terminal windows):

```
        <repo-dir> $ ./tests/client_1.py
        <repo-dir> $ ./tests/client_2.py
        <repo-dir> $ ./tests/client_3.py
```

# Running `balancer` and tests in docker #

* build project first as discribed in [Building the project](#markdown-header-building-the-project) section

* build docker image and run it

```
        <repo-dir> $ docker build --tag balancer . 
        <repo-dir> $ docker run --name balancer --rm --interactive --tty balancer
```


# Implementation notes #

Please find wiki [Implementation notes](https://bitbucket.org/gizmox/balancer/wiki/Implementation%20Notes) with additional information.
