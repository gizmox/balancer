#ifndef ROUTETABLEUTIL_H_
#define ROUTETABLEUTIL_H_

#include <string>

namespace balancer {

class RouteTable;

struct RouteTableUtil {
    static bool readRouteTable(const std::string& file_name,
                               RouteTable&        route_table);
};

} // namespace balancer

#endif /* ROUTETABLEUTIL_H_ */
