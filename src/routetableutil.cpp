#include "routetableutil.h"

#include "routetable.h"

#include <boost/tokenizer.hpp>

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

#include <fstream>
#include <iostream>

namespace balancer {

namespace {

inline
void ltrim(std::string &s)
{
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
        return !std::isspace(ch);
    }));
}

inline
void rtrim(std::string &s)
{
    s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

inline
void trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}

} // anonymous namespace

typedef boost::char_separator<char> Separator;
typedef boost::tokenizer<Separator> tokenizer;

bool RouteTableUtil::readRouteTable(const std::string& file_name,
                                    RouteTable&        route_table)
{
    log4cplus::Logger logger =
        log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("readRouteTable"));

    std::ifstream file(file_name);
    if (!file.is_open()) {
        LOG4CPLUS_ERROR(logger,
            "failed to read file "
            "'" << file_name << "'");
        return false;
    }

    std::size_t line_count = 0;
    std::string original_line;
    std::string line;
    std::vector< std::pair<RouteTable::ClientId, RouteTable::Endpoint> > table;
    while (std::getline(file, original_line)) {

        ++line_count;

        // cut comments
        std::size_t pos = original_line.find('#');
        if (std::string::npos != pos) {
            line = original_line.substr(0, pos);
        }

        trim(line);

        if (line.empty()) {
            continue;
        }

        Separator separator{":"};
        tokenizer tok{line, separator};
        tokenizer::iterator it = tok.begin();

        if (tok.end() == it) {
            LOG4CPLUS_ERROR(logger,
                "failed to parse "
                "line#=" << line_count << " "
                "'" << original_line << "'");
            return false;
        }

        const RouteTable::ClientId clientId = ::atoi(it->c_str());
        if (clientId <= 0) {
            LOG4CPLUS_ERROR(logger,
                "invalid clientId "
                "line#=" << line_count << " "
                "'" << original_line << "'");
            return false;
        }

        ++it;

        if (tok.end() == it) {
            LOG4CPLUS_ERROR(logger,
                "failed to parse "
                "line#=" << line_count << " "
                "'" << original_line << "'");
            return false;
        }

        RouteTable::Endpoint endpoint;

        endpoint.d_host = *it;
        if (endpoint.d_host.empty()) {
            LOG4CPLUS_ERROR(logger,
                "invalid host "
                "line#=" << line_count << " "
                "'" << original_line << "'");
            return false;
        }

        ++it;

        if (tok.end() == it) {
            LOG4CPLUS_ERROR(logger,
                "failed to parse "
                "line#=" << line_count << " "
                "'" << original_line << "'");
            return false;
        }

        endpoint.d_port = ::atoi(it->c_str());
        if (endpoint.d_port <= 0) {
            LOG4CPLUS_ERROR(logger,
                "invalid port "
                "line#=" << line_count << " "
                "'" << original_line << "'");
            return false;
        }

        table.push_back(
            std::pair<RouteTable::ClientId, RouteTable::Endpoint>(
                clientId, endpoint));
    }

    file.close();

    for (const auto &r : table) {
        route_table.setEndpoint(r.first, r.second);
    }

    return true;
}

} // namespace balancer
