#ifndef PROTOCOL_H_
#define PROTOCOL_H_

#include <cstddef>

namespace balancer {

// Message #1 (fixed size)
// [uint8]  : packet start = 0x42 'B'
// [uint8]  : packet type  = 0x31 '1'
// [uint32] : clientID (network byte order)

// Message #2
// [uint8]  : packet start = 0x42 'B'
// [uint8]  : packet type  = 0x32 '2'
// [uint32] : data length (network byte order)
// [bytes]  : data

struct Protocol {
    enum class ErrorCode {
        OK,
        NOT_ENOUGH_DATA,
        CORRUPTED_DATA
    };

    struct Message1 {
        unsigned int d_clientId;
    };

    static ErrorCode parseMessage1(const char* data,
                                   std::size_t data_size,
                                   Message1&   msg);
};

} // namespace balancer

#endif /* PROTOCOL_H_ */
