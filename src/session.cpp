#include "session.h"

#include "protocol.h"
#include "routetable.h"

#include <log4cplus/loggingmacros.h>

#include <functional>
#include <iostream>

namespace balancer {

using boost::asio::ip::tcp;

void Session::schedule_data_receive()
{
    d_incoming_socket.async_receive(
        boost::asio::buffer(&d_data[d_data_offset + d_data_size],
                            d_data.size() - (d_data_offset + d_data_size)),
        std::bind(&Session::on_data_received,
                  shared_from_this(),
                  std::placeholders::_1,
                  std::placeholders::_2));
}

void Session::on_data_received(boost::system::error_code ec,
                               std::size_t               bytes_received)
{
    if (ec) {
        LOG4CPLUS_ERROR(d_logger,
            "on_data_received: "
            "client#" << d_clientId << "; "
            "ec=" << ec.message() << "; "
            "bytes_received=" << bytes_received << "; ");
        return;
    }

    if (0 == bytes_received) {
        LOG4CPLUS_INFO(d_logger,
            "on_data_received: "
            "client#" << d_clientId << "; "
            "ec=" << ec.message() << "; "
            "bytes_received=" << bytes_received << "; ");
        return;
    }

    LOG4CPLUS_TRACE(d_logger,
        "on_data_received: "
        "client#" << d_clientId << "; "
        "bytes_received=" << bytes_received << "; ");

    d_data_size += bytes_received;

    if (0 == d_data_size) {
        // need more data
        schedule_data_receive();
    }
    else if (d_outcoming_socket.is_open()) {
        // outcoming connection was already established; schedule data sending
        // via outcoming connection
        schedule_data_send();
    }
    else if (!d_dst_endpoint.address().is_unspecified()) {
        // destination address was already resolved; establish outgoing
        // connection
        schedule_outcoming_connect(d_dst_endpoint);
    }
    else {
        Protocol::Message1 msg;
        switch(Protocol::parseMessage1(&d_data[d_data_offset],
                                       d_data_size,
                                       msg)) {
        case Protocol::ErrorCode::OK: {
            RouteTable::Endpoint endpoint;
            if (!d_route_table.getEndpoint(msg.d_clientId, endpoint)) {
                LOG4CPLUS_ERROR(d_logger,
                    "on_data_received: "
                    "client#" << d_clientId << "; "
                    "invalid cleintID=" << msg.d_clientId);
                return;
            }
            d_clientId = msg.d_clientId;
            LOG4CPLUS_INFO(d_logger,
                "on_data_received: "
                "clientId=" << static_cast<int>(d_clientId) << "; "
                "route=" << endpoint.d_host << ":" << endpoint.d_port);

            d_dst_endpoint.port(endpoint.d_port);

            schedule_endpoint_resolve(endpoint.d_host);
        }
        break;

        case Protocol::ErrorCode::NOT_ENOUGH_DATA:
            schedule_data_receive();
            break;

        default: // Protocol::ErrorCode::CORRUPTED_DATA
            LOG4CPLUS_ERROR(d_logger,
                "on_data_received: "
                "client#" << d_clientId << "; "
                "corrupted/unexpected data");
            break;
        };
    }
}

void Session::schedule_endpoint_resolve(const std::string& host)
{
    tcp::resolver::query query(host, std::string());
    d_resolver.async_resolve(
        query,
        std::bind(&Session::on_endpoint_resolved,
                  shared_from_this(),
                  std::placeholders::_1,
                  std::placeholders::_2));
}

void Session::on_endpoint_resolved(
                            const boost::system::error_code& ec,
                            tcp::resolver::iterator          endpoint_iterator)
{
    if (ec) {
        LOG4CPLUS_ERROR(d_logger,
            "on_endpoint_resolved: "
            "client#" << d_clientId << "; "
            "failed to resolve host name"
            "ec=" << ec.message() << "; ");
        return;
    }

    d_dst_endpoint.address(endpoint_iterator->endpoint().address());

    LOG4CPLUS_INFO(d_logger,
        "on_endpoint_resolved: "
        "client#" << d_clientId << "; "
        "resolved address=" << d_dst_endpoint.address());

    schedule_outcoming_connect(d_dst_endpoint);
}

void Session::schedule_data_send()
{
    boost::asio::async_write(d_outcoming_socket,
        boost::asio::buffer(&d_data[d_data_offset], d_data_size),
        std::bind(&Session::on_data_sent,
                  shared_from_this(),
                  std::placeholders::_1,
                  std::placeholders::_2));
}

void Session::on_data_sent(boost::system::error_code ec,
                           std::size_t               bytes_sent)
{
    if (ec) {
        LOG4CPLUS_ERROR(d_logger,
            "on_data_sent: "
            "client#" << d_clientId << "; "
            "ec=" << ec.message() << "; "
            "bytes_sent=" << bytes_sent << "; ");
        return;
    }

    LOG4CPLUS_TRACE(d_logger,
        "on_data_sent: "
        "client#" << d_clientId << "; "
        "bytes_sent=" << bytes_sent << "; ");

    d_data_offset += bytes_sent;
    d_data_size -= bytes_sent;

    if (0 == d_data_size) {
        d_data_offset = 0;
        schedule_data_receive();
    }
    else {
        schedule_data_send();
    }
}

void Session::schedule_outcoming_connect(const tcp::endpoint& endpoint)
{
    d_outcoming_socket.async_connect(
        endpoint,
        std::bind(&Session::on_outcomming_connected,
                  shared_from_this(),
                  std::placeholders::_1));
}

void Session::on_outcomming_connected(const boost::system::error_code& ec)
{
    LOG4CPLUS_INFO(d_logger,
        "on_outcomming_connected: "
        "client#" << d_clientId << "; "
        "ec=" << ec.message() << "; ");

    if (!ec) {
        // connected successfully
        schedule_data_send();
    }
    else {
        // failed to connect; try to connect again
        schedule_outcoming_connect(d_dst_endpoint);
    }
}

Session::Session(boost::asio::io_service& io_service,
                 const RouteTable&        route_table,
                 tcp::socket              incomig_socket)
: d_logger(log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("Session")))
, d_io_service(io_service)
, d_route_table(route_table)
, d_resolver(io_service)
, d_dst_endpoint()
, d_incoming_socket(std::move(incomig_socket))
, d_outcoming_socket(io_service)
, d_clientId(0)
, d_data()
, d_data_offset(0)
, d_data_size(0)
{
    LOG4CPLUS_INFO(d_logger,
        "session created");
}

Session::~Session()
{
    LOG4CPLUS_INFO(d_logger,
        "session destroyed "
        "client#" << d_clientId << "; ");
}

void Session::start()
{
    schedule_data_receive();
}

} // namespace balancer
