#ifndef SESSION_H_
#define SESSION_H_

#include <boost/asio.hpp>

#include <log4cplus/logger.h>

#include <array>
#include <string>

namespace balancer {

class RouteTable;

class Session : public std::enable_shared_from_this<Session> {
private:
    enum { DATA_BUFFER_SIZE = 1024 };

    log4cplus::Logger                  d_logger;
    boost::asio::io_service&           d_io_service;
    const RouteTable&                  d_route_table;
    boost::asio::ip::tcp::resolver     d_resolver;
    boost::asio::ip::tcp::endpoint     d_dst_endpoint;
    boost::asio::ip::tcp::socket       d_incoming_socket;
    boost::asio::ip::tcp::socket       d_outcoming_socket;
    unsigned int                       d_clientId;
    std::array<char, DATA_BUFFER_SIZE> d_data;
    std::size_t                        d_data_offset;
    std::size_t                        d_data_size;

private:
    void schedule_data_receive();
    void on_data_received(boost::system::error_code ec,
                          std::size_t               bytes_received);

    void schedule_data_send();
    void on_data_sent(boost::system::error_code ec, std::size_t bytes_sent);

    void schedule_endpoint_resolve(const std::string& endpoint);
    void on_endpoint_resolved(
                   const boost::system::error_code&         ec,
                   boost::asio::ip::tcp::resolver::iterator endpoint_iterator);

    void schedule_outcoming_connect(
                               const boost::asio::ip::tcp::endpoint& endpoint);
    void on_outcomming_connected(const boost::system::error_code& ec);

public:
    Session(boost::asio::io_service&     io_service,
            const RouteTable&            route_table,
            boost::asio::ip::tcp::socket incomig_socket);
    ~Session();

    // Session initialization is done in 'start()' method due to
    // enable_shared_from_this<>
    void start();
};

} // namespace balancer

#endif /* SESSION_H_ */
