#ifndef SERVER_H_
#define SERVER_H_

#include <boost/asio.hpp>

#include <log4cplus/logger.h>

namespace balancer {

class RouteTable;

class Server {
private:
    log4cplus::Logger              d_logger;
    boost::asio::io_service&       d_io_service;
    const RouteTable&              d_route_table;
    boost::asio::ip::tcp::acceptor d_acceptor;
    boost::asio::ip::tcp::socket   d_socket;

private:
    void schedule_accept();
    void on_incoming_connection(boost::system::error_code ec);

public:
    Server(boost::asio::io_service& io_service, const RouteTable& route_table);
};

} // namespace balancer

#endif /* SERVER_H_ */
