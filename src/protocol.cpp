#include "protocol.h"

#include <boost/endian/conversion.hpp>

#include <netinet/in.h>

namespace balancer {

namespace {
    constexpr unsigned char PACKET_START     = 0x42; // 'B'
    constexpr unsigned char PACKET_TYPE_MSG1 = 0x31; // '1'
    constexpr unsigned char PACKET_TYPE_MSG2 = 0x32; // '2'

    constexpr std::size_t PACKET_MSG1_LENGTH = 6;
}

Protocol::ErrorCode Protocol::parseMessage1(const char* data,
                                            std::size_t data_size,
                                            Message1&   msg)
{
    if (data_size < PACKET_MSG1_LENGTH) {
        return ErrorCode::NOT_ENOUGH_DATA;
    }

    if (data[0] != PACKET_START ||
        data[1] != PACKET_TYPE_MSG1) {
        return ErrorCode::CORRUPTED_DATA;
    }

    // client ID is in network order (big-endian)
    uint32_t clientId;
    memcpy(&clientId, &data[2], sizeof(uint32_t));

    // convert network byte order -> host byte order
    msg.d_clientId = boost::endian::big_to_native<uint32_t>(clientId);

    return ErrorCode::OK;
}

} // namespace balancer
