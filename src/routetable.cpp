#include "routetable.h"

#include <algorithm>
#include <functional>

namespace balancer {

RouteTable::Endpoint::Endpoint()
: d_host()
, d_port(0)
{
}

RouteTable::Endpoint::Endpoint(const Endpoint& src)
: d_host(src.d_host)
, d_port(src.d_port)
{
}

RouteTable::Endpoint::Endpoint(Endpoint&& src) noexcept
: d_host(std::move(src.d_host))
, d_port(std::move(src.d_port))
{
}

RouteTable::Endpoint::Endpoint(const std::string& host, unsigned short port)
: d_host(host)
, d_port(port)
{
}

RouteTable::Endpoint& RouteTable::Endpoint::operator=(const Endpoint& src)
{
    d_host = src.d_host;
    d_port = src.d_port;
    return *this;
}

RouteTable::Endpoint& RouteTable::Endpoint::operator=(Endpoint&& src)
{
    d_host = std::move(src.d_host);
    d_port = std::move(src.d_port);
    return *this;
}

RouteTable::RouteTable()
: d_table()
{
}

template<typename T>
bool findByClientIdPredicate(RouteTable::ClientId clientId, const T& item)
{
    return (item.first == clientId);
}

void RouteTable::setEndpoint(unsigned int clientId, const Endpoint& endpoint)
{
    Table::iterator it = std::find_if(d_table.begin(),
                                      d_table.end(),
                                      std::bind(
                                          &findByClientIdPredicate<Route>,
                                          clientId,
                                          std::placeholders::_1));
    if (it == d_table.end()) {
        d_table.push_back(Route(clientId, endpoint));
    }
    else {
        it->second = endpoint;
    }
}

bool RouteTable::getEndpoint(unsigned int clientId, Endpoint& endpoint) const
{
    Table::const_iterator it = std::find_if(d_table.cbegin(),
                                            d_table.cend(),
                                            std::bind(
                                                &findByClientIdPredicate<Route>,
                                                clientId,
                                                std::placeholders::_1));
    if (it == d_table.end()) {
        return false;
    }

    endpoint = it->second;
    return true;
}

} // namespace balancer
