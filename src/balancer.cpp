#include <iostream>

#include "server.h"
#include "routetable.h"
#include "routetableutil.h"

#include <boost/asio.hpp>

#include <log4cplus/configurator.h>
#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

#include <thread>

int main(int argc, char **argv)
{
    if (argc != 3) {
        std::cerr
            << "main: invalid number of arguments " << "\n"
               "Usage:" << "\n"
               "balancer <logging config> <route table>"
            << std::endl;
        return -1;
    }

    const std::string logging_config_file_name = argv[1];
    const std::string route_table_file_name = argv[2];

    std::cout
        << "main: command line parameters " << "\n"
           "logging config: '" << logging_config_file_name << "' " << "\n"
           "route table   : '" << route_table_file_name << "' "
        << std::endl;

    log4cplus::PropertyConfigurator::doConfigure(logging_config_file_name);
    log4cplus::Logger logger =
        log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("main"));

    balancer::RouteTable route_table;
    if (!balancer::RouteTableUtil::readRouteTable(route_table_file_name,
                                                  route_table)) {
        LOG4CPLUS_ERROR(logger,
            "failed to read route table from "
            "'" << route_table_file_name << "'");

        return -1;
    }

    try {
        boost::asio::io_service io_service;
        balancer::Server server(io_service, route_table);

        const unsigned int numCores = std::thread::hardware_concurrency();
        LOG4CPLUS_INFO(logger,
            "starting io_service threads=" << numCores);

        {
            std::thread threads[numCores];
            std::for_each(
                threads,
                threads + numCores,
                [&io_service](std::thread& t)
                {
                    t = std::move(
                        std::thread([&io_service](){ io_service.run(); }));
                });

            std::for_each(
                threads,
                threads + numCores,
                [](std::thread& t)
                {
                   t.join();
                });
        }

        LOG4CPLUS_INFO(logger,
            "io_service finished");
    }
    catch (const std::exception& e) {
        LOG4CPLUS_ERROR(logger,
            "exception: " << e.what());
    }

    return 0;
}
