#ifndef ROUTETABLE_H_
#define ROUTETABLE_H_

#include <string>
#include <vector>

namespace balancer {

class RouteTable {
public:
    typedef unsigned int ClientId;

    struct Endpoint {
        std::string    d_host;
        unsigned short d_port;

        Endpoint();
        Endpoint(const Endpoint& src);
        Endpoint(Endpoint&& src) noexcept;
        Endpoint(const std::string& host, unsigned short port);

        Endpoint& operator=(const Endpoint& src);
        Endpoint& operator=(Endpoint&& src);
    };

private:
    typedef std::pair<ClientId, Endpoint> Route;
    typedef std::vector<Route> Table;

    Table d_table;

    RouteTable(const RouteTable&) = delete;
    RouteTable& operator=(const RouteTable&) = delete;

public:
    RouteTable();

    void setEndpoint(unsigned int clientId, const Endpoint& endpoint);
    bool getEndpoint(unsigned int clientId, Endpoint& endpoint) const;
};

} // namespace balancer

#endif /* ROUTETABLE_H_ */
