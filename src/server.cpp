#include "server.h"

#include "session.h"

#include <log4cplus/loggingmacros.h>

#include <functional>
#include <iostream>
#include <memory>

namespace balancer {

using boost::asio::ip::tcp;

namespace {
    constexpr unsigned short BALANCER_INCOMING_PORT = 8888;
}

void Server::schedule_accept()
{
    d_acceptor.async_accept(d_socket,
                            std::bind(&Server::on_incoming_connection,
                                      this,
                                      std::placeholders::_1));
}

void Server::on_incoming_connection(boost::system::error_code ec)
{
    LOG4CPLUS_INFO(d_logger,
        "on_incoming_connection: "
        "ec=" << ec);

    if (!ec) {
        std::shared_ptr<Session> session =
            std::make_shared<Session>(d_io_service,
                                      d_route_table,
                                      std::move(d_socket));
        session->start();
    }

    schedule_accept();
}

Server::Server(boost::asio::io_service& io_service,
               const RouteTable&        route_table)
: d_logger(log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("Server")))
, d_io_service(io_service)
, d_route_table(route_table)
, d_acceptor(io_service, tcp::endpoint(tcp::v4(), BALANCER_INCOMING_PORT))
, d_socket(io_service)
{
    schedule_accept();
}

} // namespace balancer
